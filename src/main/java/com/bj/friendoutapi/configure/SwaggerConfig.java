package com.bj.friendoutapi.configure;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@OpenAPIDefinition(
        info = @Info(title = "손절장인  App",
                description = "선물기록으로 마음에 안드는 사람을 손절시키자",
                version = "v1"))
@RequiredArgsConstructor
@Configuration

public class SwaggerConfig {
    @Bean
    public GroupedOpenApi v1OpenApi() {
        String[] paths = {"/v1/**"};

        return GroupedOpenApi.builder()
                .group("손절장인 API v1")
                .pathsToMatch(paths)
                .build();
    }
}
