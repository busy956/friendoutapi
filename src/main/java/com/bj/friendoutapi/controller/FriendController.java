package com.bj.friendoutapi.controller;

import com.bj.friendoutapi.model.friend.FriendChangeRequest;
import com.bj.friendoutapi.model.friend.FriendCreateRequest;
import com.bj.friendoutapi.model.friend.FriendItem;
import com.bj.friendoutapi.model.friend.FriendResponse;
import com.bj.friendoutapi.service.FriendService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/friend")
public class FriendController {
    private final FriendService friendService;

    @PostMapping("/join")
    public String setFriend(@RequestBody FriendCreateRequest request) {
        friendService.setFriend(request);

        return "OK";
    }

    @GetMapping("/all")
    public List<FriendItem> getFriends() {
        return friendService.getFriends();
    }

    @GetMapping("/detail/{friendId}")
    public FriendResponse getFriend(@PathVariable long friendId){
       return friendService.getFriend(friendId);
    }

    @PutMapping("/Memo/friend-id/{friendId}")
    public String putFriend(@PathVariable long friendId, @RequestBody FriendChangeRequest request) {
        friendService.putMemo(friendId, request);

        return "OK";
    }
}

