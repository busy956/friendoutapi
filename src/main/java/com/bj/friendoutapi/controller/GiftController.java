package com.bj.friendoutapi.controller;

import com.bj.friendoutapi.entity.Friend;
import com.bj.friendoutapi.model.gift.GiftChangeRequest;
import com.bj.friendoutapi.model.gift.GiftCreateRequest;
import com.bj.friendoutapi.model.gift.GiftItem;
import com.bj.friendoutapi.model.gift.GiftResponse;
import com.bj.friendoutapi.service.FriendService;
import com.bj.friendoutapi.service.GiftService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/gift")
public class GiftController {
    private final GiftService giftService;
    private final FriendService friendService;

    @PostMapping("/join/friend-Id/{friendId}")
    public String setGift(@PathVariable long friendId, @RequestBody GiftCreateRequest request) {
        Friend friend = friendService.getData(friendId);
        giftService.setGift(friend, request);

        return "OK";
    }

    @GetMapping("/all")
    public List<GiftItem> getGifts() {
        return giftService.getGifts();
    }

    @GetMapping("/detail/{giftId}")
    public GiftResponse getGift(@PathVariable long giftId) {
        return giftService.getGift(giftId);
    }

    @PutMapping("/content/gift-id/{giftId}")
    public String putGift(@PathVariable long giftId, @RequestBody GiftChangeRequest request) {
        giftService.putContent(giftId, request);

        return "OK";
    }
}
