package com.bj.friendoutapi.entity;

import com.bj.friendoutapi.enums.GiveStatus;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Gift {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "friednId", nullable = false)
    private Friend friend;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private GiveStatus type;

    @Column(nullable = false)
    private String content;

    @Column(nullable = false)
    private Double price;

    @Column(nullable = false)
    private LocalDate giftDate;
}
