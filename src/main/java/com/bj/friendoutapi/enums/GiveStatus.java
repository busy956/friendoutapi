package com.bj.friendoutapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum GiveStatus {
    ZUM("줌"),
    bat_dem("받음");

    private final String giveAndTake;
}
