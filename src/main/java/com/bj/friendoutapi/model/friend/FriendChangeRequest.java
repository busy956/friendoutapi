package com.bj.friendoutapi.model.friend;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FriendChangeRequest {
    private String etcMemo;
}
