package com.bj.friendoutapi.model.friend;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FriendCreateRequest {
    private String name;
    private String birthDay;
    private String phoneNumber;
    private String etcMemo;
    private Boolean isCutFriend;
}
