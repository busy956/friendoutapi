package com.bj.friendoutapi.model.friend;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FriendItem {
    private Long id;
    private String name;
    private Boolean isCutFriend;
}
