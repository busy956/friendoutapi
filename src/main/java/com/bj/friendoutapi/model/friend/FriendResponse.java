package com.bj.friendoutapi.model.friend;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class FriendResponse {
    private Long id;
    private String name;
    private String birthDay;
    private String phoneNumber;
    private String etcMemo;
    private Boolean isCutFriend;
    private LocalDate cutData;
    private String cutReason;
}
