package com.bj.friendoutapi.model.gift;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GiftChangeRequest {
    private String content;
}
