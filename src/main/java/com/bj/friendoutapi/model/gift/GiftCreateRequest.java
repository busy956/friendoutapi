package com.bj.friendoutapi.model.gift;

import com.bj.friendoutapi.entity.Friend;
import com.bj.friendoutapi.enums.GiveStatus;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class GiftCreateRequest {
    private GiveStatus type;
    private String content;
    private Double price;
    private LocalDate giftDate;
}
