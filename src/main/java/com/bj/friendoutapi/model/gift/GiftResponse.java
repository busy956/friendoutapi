package com.bj.friendoutapi.model.gift;

import com.bj.friendoutapi.entity.Friend;
import com.bj.friendoutapi.enums.GiveStatus;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class GiftResponse {
    private Long id;
    private String friendName;
    private String friendBirthDay;
    private String friendPhoneNumber;
    private String friendEtcMemo;
    private Boolean friendIsCutFriend;
    private LocalDate friendCutData;
    private String friendCutReason;
    private GiveStatus type;
    private String content;
    private Double price;
    private LocalDate giftDate;
}
