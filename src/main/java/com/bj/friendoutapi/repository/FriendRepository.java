package com.bj.friendoutapi.repository;

import com.bj.friendoutapi.entity.Friend;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FriendRepository extends JpaRepository<Friend, Long> {
}
