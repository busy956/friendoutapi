package com.bj.friendoutapi.repository;

import com.bj.friendoutapi.entity.Gift;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GiftRepository extends JpaRepository<Gift, Long> {
}
