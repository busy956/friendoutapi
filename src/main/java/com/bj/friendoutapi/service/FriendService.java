package com.bj.friendoutapi.service;

import com.bj.friendoutapi.entity.Friend;
import com.bj.friendoutapi.model.friend.FriendChangeRequest;
import com.bj.friendoutapi.model.friend.FriendCreateRequest;
import com.bj.friendoutapi.model.friend.FriendItem;
import com.bj.friendoutapi.model.friend.FriendResponse;
import com.bj.friendoutapi.repository.FriendRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class FriendService {
    private final FriendRepository friendRepository;

    public Friend getData(long id) {
        return friendRepository.findById(id).orElseThrow();
    }

    public void setFriend(FriendCreateRequest request) {
        Friend addData = new Friend();
        addData.setName(request.getName());
        addData.setBirthDay(request.getBirthDay());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setEtcMemo(request.getEtcMemo());
        addData.setIsCutFriend(request.getIsCutFriend());

        friendRepository.save(addData);
    }

    public List<FriendItem> getFriends() {
        List<Friend> originData = friendRepository.findAll();

        List<FriendItem> result = new LinkedList<>();

        for (Friend friend : originData) {
            FriendItem addItem = new FriendItem();
            addItem.setId(friend.getId());
            addItem.setName(friend.getName());
            addItem.setIsCutFriend(friend.getIsCutFriend());

            result.add(addItem);
        }
        return result;
    }

    public FriendResponse getFriend(long id) {
        Friend originData = friendRepository.findById(id).orElseThrow();
        FriendResponse response = new FriendResponse();
        response.setId(originData.getId());
        response.setName(originData.getName());
        response.setBirthDay(originData.getBirthDay());
        response.setPhoneNumber(originData.getPhoneNumber());
        response.setEtcMemo(originData.getEtcMemo());
        response.setIsCutFriend(originData.getIsCutFriend());
        response.setCutData(originData.getCutData());

        return response;
    }

    public void putMemo(long id, FriendChangeRequest request) {
        Friend originData = friendRepository.findById(id).orElseThrow();
        originData.setEtcMemo(request.getEtcMemo());

        friendRepository.save(originData);

    }
}
