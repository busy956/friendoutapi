package com.bj.friendoutapi.service;

import com.bj.friendoutapi.entity.Friend;
import com.bj.friendoutapi.entity.Gift;
import com.bj.friendoutapi.model.gift.GiftChangeRequest;
import com.bj.friendoutapi.model.gift.GiftCreateRequest;
import com.bj.friendoutapi.model.gift.GiftItem;
import com.bj.friendoutapi.model.gift.GiftResponse;
import com.bj.friendoutapi.repository.GiftRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.lang.reflect.Member;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class GiftService {
    private final GiftRepository giftRepository;

    public void setGift(Friend friend, GiftCreateRequest request) {
        Gift addData = new Gift();
        addData.setFriend(friend);
        addData.setType(request.getType());
        addData.setContent(request.getContent());
        addData.setPrice(request.getPrice());
        addData.setGiftDate(request.getGiftDate());

        giftRepository.save(addData);
    }

    public List<GiftItem> getGifts() {
        List<Gift> originList = giftRepository.findAll();

        List<GiftItem> result = new LinkedList<>();

        for (Gift gift : originList) {
            GiftItem addItem = new GiftItem();
            addItem.setId(gift.getId());
            addItem.setFriendName(gift.getFriend().getName());
            addItem.setFriendBirthDay(gift.getFriend().getBirthDay());
            addItem.setFriendPhoneNumber(gift.getFriend().getPhoneNumber());
            addItem.setFriendEtcMemo(gift.getFriend().getEtcMemo());
            addItem.setFriendIsCutFriend(gift.getFriend().getIsCutFriend());
            addItem.setFriendCutData(gift.getFriend().getCutData());
            addItem.setFriendCutReason(gift.getFriend().getCutReason());
            addItem.setType(gift.getType());
            addItem.setContent(gift.getContent());

            result.add(addItem);
        }
        return result;
    }

    public GiftResponse getGift(long id) {
        Gift originData = giftRepository.findById(id).orElseThrow();
        GiftResponse response = new GiftResponse();
        response.setId(originData.getId());
        response.setFriendName(originData.getFriend().getName());
        response.setFriendBirthDay(originData.getFriend().getBirthDay());
        response.setFriendPhoneNumber(originData.getFriend().getPhoneNumber());
        response.setFriendEtcMemo(originData.getFriend().getEtcMemo());
        response.setFriendIsCutFriend(originData.getFriend().getIsCutFriend());
        response.setFriendCutData(originData.getFriend().getCutData());
        response.setFriendCutReason(originData.getFriend().getCutReason());
        response.setType(originData.getType());
        response.setContent(originData.getContent());
        response.setPrice(originData.getPrice());
        response.setGiftDate(originData.getGiftDate());

        return response;
    }

    public void putContent(long id, GiftChangeRequest request) {
        Gift originData = giftRepository.findById(id).orElseThrow();
        originData.setContent(request.getContent());

        giftRepository.save(originData);
    }
}
